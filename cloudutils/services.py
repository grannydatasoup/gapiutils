from googleads import oauth2
from googleads import adwords
from googleapiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials
import yaml
from yaml import Loader
from gcloud import datastore

__author__ = 'nkdhny'

'''    
    auth:
        cert: path to pem file
        password: notasecret
        email: user_email

    service_name:
        version: default version to use
        scope:  #set of default scopes
        - scope_1
        - scope_2
        - ...
        - scope_n
'''


def _generic_service(service_name, service_version=None, cert_file=None, cert_password=None, user_email=None,
                     scope=None, conf=None):
    cert_file = cert_file or conf['gapi']['auth']['cert']
    cert_password = (cert_password or conf['gapi']['auth']['cert']) or 'notasecret'
    user_email = user_email or conf['gapi']['auth']['email']
    service_version = service_version or conf['gapi'][service_name]['version']
    scope = ' '.join(scope or conf['gapi'][service_name]['scope'])

    credentials = ServiceAccountCredentials.from_p12_keyfile(
        user_email, cert_file, private_key_password=cert_password, scopes=scope)

    return build(service_name, service_version, credentials=credentials)


def build_adwords_client(refresh_token, client_customer_id, conf=None, conf_file=None):
    conf_file = conf_file or '/etc/gaw/gapi.yaml'
    conf = conf or yaml.load(file(conf_file), Loader)

    developer_token = conf['gapi']['auth']['adwords']['devtoken']
    user_agent = conf['gapi']['auth']['adwords']['agent']
    client_id = conf['gapi']['auth']['client']['id']
    client_secret = conf['gapi']['auth']['client']['secret']

    oauth2_client = oauth2.GoogleRefreshTokenClient(
        client_id, client_secret, refresh_token)

    return adwords.AdWordsClient(
        developer_token, oauth2_client, user_agent, client_customer_id)


def build_task_queue_service(consumer_only=True, version='v1beta2', conf=None, conf_file='/etc/gaw/gapi.yaml'):
    conf = conf or yaml.load(file(conf_file), Loader)
    if consumer_only:
        scope = ['https://www.googleapis.com/auth/taskqueue.consumer']
    else:
        scope = ['https://www.googleapis.com/auth/taskqueue']

    return _generic_service('taskqueue', service_version=version, scope=scope, conf=conf)


def build_metrics_service(version='v2beta2', conf=None, conf_file='/etc/gaw/gapi.yaml'):
    conf = conf or yaml.load(file(conf_file), Loader=Loader)

    return _generic_service('cloudmonitoring', version, scope=['https://www.googleapis.com/auth/monitoring'], conf=conf)


def authorize_datastore(dataset=None, conf=None, conf_file='/etc/gaw/gapi.yaml'):
    conf = conf or yaml.load(file(conf_file), Loader)

    cert_file = conf['gapi']['auth']['cert']
    cert_password = conf['gapi']['auth']['cert'] or 'notasecret'
    user_email = conf['gapi']['auth']['email']
    dataset = dataset or conf['gapi']['app']

    scope = 'https://www.googleapis.com/auth/datastore https://www.googleapis.com/auth/userinfo.email'

    credentials = ServiceAccountCredentials.from_p12_keyfile(
        user_email, cert_file, private_key_password=cert_password, scopes=scope)

    connection = datastore.Connection(credentials=credentials)
    datastore.set_default_connection(connection)
    datastore.set_default_dataset_id(dataset)

    return connection
