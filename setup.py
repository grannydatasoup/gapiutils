from distutils.core import setup

setup(name='GoogleCloudUtils',
      version='0.1',
      description='Utility functions to make usage of GAPI easier',
      author='Alexey Golomedov',
      author_email='golomedov@gmail.com',
      url='https://github.com/nkdhny/gapiutils',
      packages=['cloudutils'],
      install_requires=['pyyaml', 'oauth2client', 'google-api-python-client', 'gcloud', 'googleads']

      )
